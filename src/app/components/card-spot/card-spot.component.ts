import { Component, OnInit } from '@angular/core';
import { Card, UnitCard } from 'src/app/models/card.model';
import { ActiveCardService } from 'src/app/services/active-card.service';
import { skip, take } from 'rxjs/operators';

@Component({
  selector: 'card-spot',
  templateUrl: './card-spot.component.html',
  styleUrls: ['./card-spot.component.scss']
})
export class CardSpotComponent implements OnInit {

  activeCard: Card;
  uuid: string;
  id: string;

  displayActionMenu: boolean = false;
  myCardIsActive: boolean = false;

  constructor(private activeCardService: ActiveCardService) { }

  ngOnInit() {
    this.activeCardService.activeCard.subscribe(arg => {
      if (arg.card) {
        this.myCardIsActive = this.uuid == arg.getUuid();
      }
    });
  }

  spotClick() {
    event.stopPropagation();
    if (this.activeCardService.curCard.getUuid() === '-1') return;
    if (!(this.activeCardService.curCard instanceof UnitCard)) {
      console.log('Only Unit Cards can be placed here, homie');
      return;
    };

    this.myCardIsActive = (this.uuid === this.activeCardService.curCard.getUuid());

    if (!this.activeCard && this.activeCardService.curCard) {
      this.displayActionMenu = false;
      console.log('----------- Setting ActiveCard ------------------')
      this.activeCard = this.activeCardService.curCard;
      this.uuid = this.activeCard.getUuid();
      this.id = this.activeCard.card.id;
    }
    else {
      console.log('----------- Already Active ------------------')
      this.displayActionMenu = true;

    }
  }

  async equipItem() {
    event.stopPropagation();

    console.log("Equipping something!!!!");
    await this.activeCardService.getActiveCard().toPromise().then(card => {
      console.log("Equpping card: " + card.card.name);
    });
    // (this.activeCard as UnitCard).equipItem()
  }

  clearSpot() {
    event.stopPropagation();

    console.log('Trying to clear this card ' + this.uuid)
    this.myCardIsActive = false;
    this.displayActionMenu = false;
    this.activeCard = null;
    this.uuid = null;
    this.id = null;
    // this.ngOnInit();
  }

  attack() {
    console.log("Attacking");
    event.stopPropagation();

  }


  switch() {
    console.log("Switching");
    event.stopPropagation();
    this.activeCardService.activeCard.pipe(skip(1)).pipe(take(1)).subscribe(arg => {
      console.log(" Hey WE ARE SUBSCRIBING RN");
      console.log(arg.getUuid());
      // let argCopy= arg;
      // arg = this.activeCard;
      // this.activeCard = argCopy;

      this.activeCard = this.activeCardService.curCard;
      this.uuid = this.activeCard.getUuid();
      this.id = this.activeCard.card.id;
    });
  }

}


