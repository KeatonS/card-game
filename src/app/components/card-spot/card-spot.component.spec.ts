import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSpotComponent } from './card-spot.component';

describe('CardSpotComponent', () => {
  let component: CardSpotComponent;
  let fixture: ComponentFixture<CardSpotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSpotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
