import { Component, OnInit } from '@angular/core';
import { Game } from 'src/app/models/game.model';

@Component({
  selector: 'game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss']
})
export class GameBoardComponent implements OnInit {

  public game: Game; // The main data structure holding info about the game

  constructor() { }

  ngOnInit() {
  }

  gameSetup(){
    // Make player 1
    // Make player 2
  }
}
