import { Component, OnInit, Input } from '@angular/core';
import { Unit } from 'src/app/models/unit.model';
import { HttpClient } from '@angular/common/http';
import * as uuid from 'uuid';
import { GameStateMachineService } from 'src/app/services/game-manager.service';
import { UnitCard } from 'src/app/models/card.model';

@Component({
  selector: 'unit-card',
  templateUrl: './unit-card.component.html',
  styleUrls: ['./unit-card.component.scss']
})
export class UnitCardComponent implements OnInit {

  @Input() unitCard: UnitCard;
  
  @Input() unitId: number;
  @Input() uuid: string;
  @Input() curHealth: number;

  constructor(private http: HttpClient, private gms: GameStateMachineService) {

  }

  ngOnInit() {
    // this.http.get('/assets/units.json').subscribe(
    //   (units: Unit[]) => {
    //     this.unitCard.unit = units.find(x => x.id == this.unitId);

    //     if(this.unitCard.getUuid()) {
    //       this.unitCard = this.unitCard;
    //     }

    //     if (!this.curHealth) {
    //       this.unitCard.curHealth = this.unitCard.unit.stats.health;
    //     }else{
    //       this.unitCard.curHealth = this.curHealth;
    //     }
    //     if (!this.uuid) {
    //       this.unitCard.setUuid(uuid.v1());
    //     } else{
    //       this.unitCard.setUuid(this.uuid);
    //     }
    //   });
  }

  
  onClickCard(){
    this.gms.setActivePlayerCard(this.unitCard);
  }

}
