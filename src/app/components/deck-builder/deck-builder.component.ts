import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import { ActiveCardService } from 'src/app/services/active-card.service';
import { CardDataService } from 'src/app/services/card-data.service';
import { CardNameSearchPipe } from './../../pipes/card-name-search.pipe';

@Component({
  selector: 'deck-builder',
  templateUrl: './deck-builder.component.html',
  styleUrls: ['./deck-builder.component.scss']
})
export class DeckBuilderComponent implements OnInit {
  searchText: string = "";
  allCardsList: Card[];

  constructor(private cardDataService: CardDataService, private activeCardService: ActiveCardService) {
  }

  ngOnInit() {
    this.allCardsList = this.cardDataService.getAllCards();
    console.log(this.allCardsList[0].card.id);
  }

  //Clear term types by user
  clearFilter() {
    this.searchText = "";
  }
}
