import { Component, OnInit } from '@angular/core';
import { PlayerSaveData, GameSaveService } from 'src/app/services/game-save.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'card-deck',
  templateUrl: './card-deck.component.html',
  styleUrls: ['./card-deck.component.scss']
})
export class CardDeckComponent implements OnInit {

  public saveCards: PlayerSaveData;

  constructor(private gameSaveService: GameSaveService) {
    this.gameSaveService.getPlayerSaveDataSubject().subscribe(data => {
      this.saveCards = data;
    });
  }

  ngOnInit() {

  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.saveCards.unitSaves, event.previousIndex, event.currentIndex);
  }

}
