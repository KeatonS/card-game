import { Component, Input, OnInit } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import { ActiveCardService } from 'src/app/services/active-card.service';
import { CardDataService } from 'src/app/services/card-data.service';

@Component({
  selector: 'card-list-item',
  templateUrl: './card-list-item.component.html',
  styleUrls: ['./card-list-item.component.scss']
})
export class CardListItemComponent implements OnInit {

  @Input() id: string;
  @Input() uuid: string;
  thisCard: Card;
  isActiveCard: boolean = false;

  constructor(private cardDataService: CardDataService, private activeCardService: ActiveCardService) {
  }

  ngOnInit() {
    this.activeCardService.activeCard.subscribe(arg => {
      if (arg.card && this.thisCard) {
        console.log("Single Card Component!");
        this.isActiveCard = this.thisCard.getUuid() === arg.getUuid();
      }
    });
    this.thisCard = this.cardDataService.getCardTs({ id: this.id, uuid: this.uuid });
  }

  ngOnChanges(changes) {
    this.ngOnInit();
  }

  setActiveCard() {
    this.activeCardService.setActiveCard(this.thisCard);
  }
}
