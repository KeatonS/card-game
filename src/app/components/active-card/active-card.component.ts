import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import { ActiveCardService } from 'src/app/services/active-card.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'active-card',
  templateUrl: './active-card.component.html',
  styleUrls: ['./active-card.component.scss']
})
export class ActiveCardComponent implements OnInit {

  activeCard: Card;
  uuid: string;
  id: string;
  constructor(private activeCardService: ActiveCardService) { }

  ngOnInit() {
    this.activeCardService.activeCard.subscribe(arg => {
      if(arg.card){
        console.log("Active Card Component!");
        this.activeCard = arg;
        this.uuid = this.activeCard.getUuid();
        this.id = this.activeCard.card.id;
      }
    });
  }

}
