import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Card } from '../models/card.model';

@Injectable({
  providedIn: 'root'
})
export class ActiveCardService {

  constructor() { }
  curCard: Card = new Card(null, '-1');
  private activeCardSubject = new BehaviorSubject<Card>(this.curCard);
  private prom = new Promise<Card>(null);

  activeCard = this.activeCardSubject.asObservable();

  public setActiveCard(card: Card) {
    if (this.curCard.getUuid() != card.getUuid()) {
      this.activeCardSubject.next(card);
      this.prom;
      this.curCard = card;
      console.log("Card has been set in service!!");
    }
  }

  public getActiveCard(): Observable<Card> {
    return this.activeCardSubject.asObservable();
  }

  public getActiveCardProm(): Promise<Card> {
    return this.activeCardSubject.toPromise();
  }
}
