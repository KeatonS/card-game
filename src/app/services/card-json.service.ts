import { Injectable } from '@angular/core';
import ArmsJson from '../../assets/equippable-items/weapons/weapons.json'
import ToolsJson from '../../assets/equippable-items/tools/tools.json'
import UnitsJson from '../../assets/units/units.json'

import { Arm, Tool, iCard, Card } from '../models/card.model';
import { Unit } from '../models/unit.model';
import { getUnit } from 'src/assets/cards/units';
@Injectable({
  providedIn: 'root'
})
export class CardJsonService {

  allUnits: Unit[] = JSON.parse(JSON.stringify(UnitsJson));
  allArms: Arm[] = JSON.parse(JSON.stringify(ArmsJson));
  allTools: Tool[] = JSON.parse(JSON.stringify(ToolsJson));

  constructor() {
  }


  getCardByID(cardSource: CardTypes, id: string | number) {
    let listToSearch = [];
    switch (cardSource) {
      case CardTypes.UNITS:
        listToSearch = this.allUnits;
        break;
      case CardTypes.ARMS:
        listToSearch = this.allArms;
        break;
      case CardTypes.TOOLS:
        listToSearch = this.allTools;
        break;
      case null:
        listToSearch = [].concat(...this.allUnits, ...this.allTools, ...this.allArms);//(this.allUnits, this.allArms, this.allTools);
        break;
    }

    if (!listToSearch) {
      console.error("Error trying to access an invalid JSON file or Card type");
      return null;
    }
    return listToSearch.find(card => card.id.toString() == id.toString());

  }


}

export enum CardTypes {
  UNITS,
  ARMS,
  TOOLS
}
