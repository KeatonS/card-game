import { TestBed } from '@angular/core/testing';
import PlayerOneJson from '../../assets/player-data/player-one-data.json'
import { CardDataService } from './card-data.service';
import { CardSaveData } from '../models/player-data.model';
import { UnitCard } from '../models/card.model';

describe('PlayerDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CardDataService = TestBed.get(CardDataService);
    expect(service).toBeTruthy();
  });

  it('should make a deck from data', () => {
    const service: CardDataService = TestBed.get(CardDataService);
    let p1Data: CardSaveData[] = JSON.parse(JSON.stringify(PlayerOneJson));

    let p1Deck = service.convertPlayerDataToCards(p1Data);
    expect(p1Deck.cards[0].getUuid()).toEqual('aaa');
  });

  // it('should make a deck from data and check the card data', () => {
  //   const service: PlayerDataService = TestBed.get(PlayerDataService);
  //   let p1Data: CardSaveData[] = JSON.parse(JSON.stringify(PlayerOneJson));

  //   let p1Deck = service.convertPlayerDataToCards(p1Data);
  //   expect(p1Deck.cards[0].card.name).toEqual('Jimmy Sage Boy');

  // });
});
