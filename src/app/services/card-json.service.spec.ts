import { TestBed } from '@angular/core/testing';

import { CardJsonService, CardTypes } from './card-json.service';
import { Unit } from '../models/unit.model';
import { Swordsman, Sage, UnitCard, ArmCard, ArmsTypes, ArmsClasses, ToolClasses, ArmsClass, ToolCard, HealToolCard, ToolTypes } from '../models/card.model';

describe('CardJsonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CardJsonService = TestBed.get(CardJsonService);
    expect(service).toBeTruthy();
  });

  it('should get the first arms data', () => {
    const service: CardJsonService = TestBed.get(CardJsonService);
    expect(service.allArms[0].name).toEqual('CRAZY-ASS-MAGIC');
  });

  it('should get an arm by its ID', () => {
    const service: CardJsonService = TestBed.get(CardJsonService);
    expect(service.getCardByID(CardTypes.ARMS, "2").name).toEqual('Bitchy Sword');
  });

  it('should take the first arms data, create a card, and give it to a unit', () => {
    const service: CardJsonService = TestBed.get(CardJsonService);
    let unit = new UnitCard(getSwordsManUnit());
    let unit2 = new UnitCard(getSwordsManUnit());

    expect(unit.equipItem(new ArmCard(service.getCardByID(CardTypes.ARMS, "2")))).toBeTruthy();
    expect(unit.equipItem(new ArmCard(service.getCardByID(CardTypes.ARMS, "1")))).toBeTruthy();

    expect(unit.getEquippableCard(0).card.name).toEqual('Bitchy Sword');
    expect(unit.getEquippableCard(0).card["armsType"]).toEqual(ArmsTypes.SWORD);
    expect(unit.useItemOnTarget(unit.getEquippableCard(0), unit2)).toBeTruthy();

    expect(unit.useItemOnTarget(1, unit)).toBeFalsy();
  });


  it('should get a card by its ID using the generic method', () => {
    const service: CardJsonService = TestBed.get(CardJsonService);
    expect(service.getCardByID(CardTypes.ARMS, "2").name).toEqual('Bitchy Sword');
    expect(service.getCardByID(CardTypes.TOOLS, "1").name).toEqual('Healing Staff');
    expect(service.getCardByID(CardTypes.TOOLS, "1").healAmount).toEqual(20);
    expect(service.getCardByID(CardTypes.UNITS, "1").name).toEqual('Jimmy Sage Boy');
  });


  it('get unit card by its ID and check it ', () => {
    const service: CardJsonService = TestBed.get(CardJsonService);
    expect(service.getCardByID(CardTypes.ARMS, "2").name).toEqual('Bitchy Sword');
    let unit = new UnitCard(service.getCardByID(CardTypes.UNITS, "1"));
    expect(unit.getUnit().name).toEqual('Jimmy Sage Boy');
    expect(unit.getArmsClass().className).toEqual("Swordsman");
    expect(unit.getToolsClass().className).toEqual("Sage");
    expect(unit.equipItem(new ArmCard(service.getCardByID(CardTypes.ARMS, "1")))).toBeTruthy();
    expect(unit.useItemOnTarget(0, unit)).toBeFalsy();
    expect(unit.equipItem(new ArmCard(service.getCardByID(CardTypes.ARMS, "2")))).toBeTruthy();
    expect(unit.useItemOnTarget(1, unit)).toBeTruthy();
    expect(unit.getCurHealth()).toEqual(77);
    expect(unit.equipItem(new HealToolCard(service.getCardByID(CardTypes.TOOLS, "1")))).toBeTruthy();
    expect(unit.getToolsClass().toolsList.includes(ToolTypes.STAFF)).toBeTruthy();
    expect(unit.getEquippableCard(2).card.name).toEqual('Healing Staff');
    expect(unit.useItemOnTarget(2, unit)).toBeTruthy();
    expect(unit.getCurHealth()).toEqual(97);
  });

});


function getSwordsManUnit() {
  let unit: Unit = {
    name: "p1 Jimmy Mage 0",
    id: "1",
    stats: {
      attack: 5,
      defense: 2,
      health: 100,
      luck: 5,
      skill: 5,
    },
    armsClass: ArmsClasses.SWORDSMAN,
    toolClass: ToolClasses.SAGE
  }

  return unit;
}
