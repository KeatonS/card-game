import { TestBed } from '@angular/core/testing';

import { GameStateMachineService } from './game-manager.service';

describe('GameManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameStateMachineService = TestBed.get(GameStateMachineService);
    expect(service).toBeTruthy();
  });
});
