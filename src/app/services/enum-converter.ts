import {JsonCustomConvert,JsonConverter} from "json2typescript";
import { Equippable, ArmsTypes } from '../models/card.model';

@JsonConverter
export class EnumConverter<T> implements JsonCustomConvert<any> {
  enumType: {[key: string]: any};

  constructor(enumType: {[key: string]: any}) {
    this.enumType = enumType;
  }

  serialize(data: any): string {
    if (!data) {
      return null;
    }
    return data.toString(); // Return as a number if that is desired
  }

  deserialize(data: any): T {
    if (data === undefined || data == null) {
      return undefined;
    }
    for (const property of Object.keys(this.enumType)) {
      if (property.toUpperCase() === String(data).toUpperCase()) {
        const enumMember = this.enumType[property];
        if (typeof enumMember === 'string') {
          return <T>this.enumType[<string>enumMember];
        }
      }
    }
    return undefined;
  }
}

export interface NewArm extends Equippable {
    armsClass: ArmsTypes;
    maxRange: number;
    minRange: number;
    damage: number;
}