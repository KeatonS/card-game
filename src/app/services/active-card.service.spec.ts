import { TestBed } from '@angular/core/testing';

import { ActiveCardService } from './active-card.service';

describe('ActiveCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActiveCardService = TestBed.get(ActiveCardService);
    expect(service).toBeTruthy();
  });
});
