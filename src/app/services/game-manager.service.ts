import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Card } from '../models/card.model';

@Injectable({
  providedIn: 'root'
})
export class GameStateMachineService {

  private activePlayerCardSubject = new BehaviorSubject<Card>(new Card(null, '-1'));
  private activeCard: Card;

  constructor() { }

  public setActivePlayerCard(card: Card){
    this.activeCard = card;
    this.activePlayerCardSubject.next(card);
  }
  public getActivePlayerCard(){
    return this.activePlayerCardSubject.asObservable();
  }

  public getActiveCard(){
    return this.activeCard;
  }
}
