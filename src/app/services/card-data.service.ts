import { Injectable } from '@angular/core';
import { CardJsonService } from './card-json.service';
import { Board, Deck, Player, PlayerCards } from '../models/game.model';
import { CardSaveData } from '../models/player-data.model';
import { Card, Equippable, EquippableCard, iCard, UnitCard } from '../models/card.model';
import { Unit, UnitStats } from '../models/unit.model';
import { getUnit } from 'src/assets/cards/units';
import { getTool } from 'src/assets/cards/tools';
import { getArm } from 'src/assets/cards/arms';

/**
 * This service is for pulling up a player's data before a match and
 * turn his/her card data into an array of Card instances to be used in a match.
 */


@Injectable({
  providedIn: 'root'
})
export class CardDataService {

  constructor(protected cardJsonService: CardJsonService) { }

  public getBoard(p1: object, p2: object): Board {
    let p1Data: CardSaveData[] = JSON.parse(JSON.stringify(p1));
    let p2Data: CardSaveData[] = JSON.parse(JSON.stringify(p2));
    let p1Deck: Deck = this.convertPlayerDataToCards(p1Data);
    let p2Deck: Deck = this.convertPlayerDataToCards(p2Data);

    let shipHealth = 50;
    let gameBoard: Board = new Board(new Player(shipHealth, new PlayerCards(p1Deck)), new Player(shipHealth, new PlayerCards(p2Deck)));

    return gameBoard;
  }

  convertPlayerDataToCards(playerSaveData: CardSaveData[]): Deck {
    let deck: Deck = new Deck([]);
    playerSaveData.forEach((cardData: CardSaveData) => {
      let newCard = this.getCardTs(cardData);
      deck.cards.push(newCard);
    });

    return deck;
  }

  isUnit(card: iCard): card is Unit {
    return 'armsClass' in card;
  }

  isEquippable(card: iCard): card is Equippable {
    return 'uses' in card;
  }


  public getCardTs(saveData: CardSaveData): Card {
    let card: Card;
    switch (saveData.id.substr(0, 2)) {
      case '01':
        card = getUnit(saveData);
        break;
      case '02':
        card = getTool(saveData);
        break;
      case '03':
        card = getArm(saveData);
        break;
    }
    return card;
  }

  public getAllCards(): Card[] {
    let returnCards: Card[] = [];
    for (let i = 1; i <= 3; i += 1) {
      const cardType = i.toString().padStart(2, '0')
      let cardsOfType = this.allCardsForType(cardType);
      console.log(cardsOfType.length)
      returnCards.push.apply(returnCards, cardsOfType);
    }
    console.log(returnCards)
    return returnCards;

  }

  allCardsForType(typeCode: string): Card[] {
    let returnCards: Card[] = [];
    for (let i = 1; i < 1000; i += 1) {
      const id = typeCode + i.toString().padStart(3, '0')
      console.log(id);
      const newCard = this.getCardTs({ id: id, uuid: null });
      if (newCard.getUuid() == '-1') break;
      else {
        returnCards.push(newCard);
      }
    }

    return returnCards;
  }
}
