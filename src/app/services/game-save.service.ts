import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Unit } from 'src/app/models/unit.model';
import { UnitCardComponent } from 'src/app/components/unit-card/unit-card.component';

@Injectable({
  providedIn: 'root'
})
export class GameSaveService {

  public saveData: PlayerSaveData;
  private playerSaveDataSubject = new BehaviorSubject<PlayerSaveData>(null);

  constructor() {
    localStorage.setItem('key', 'value');
    //   let data = `{
    //     "unitSaves": [
    //         {
    //           "uuid": "xxx",
    //           "cardId": "1",
    //           "curHealth": "59"
    //         }
    //       ]
    // }
    //   `;

    //   localStorage.setItem('saveData', data);
    this.setPlayerSaveDataSubject(JSON.parse(localStorage.getItem('saveData')));
    this.getPlayerSaveDataSubject().subscribe(data => {
      this.saveData = data;
      // console.log(this.saveData);
    });

  }

  public setPlayerSaveDataSubject(playerSaveData: PlayerSaveData) {
    this.playerSaveDataSubject.next(playerSaveData);
  }
  public getPlayerSaveDataSubject() {
    return this.playerSaveDataSubject.asObservable();
  }

  // public saveCard(unitCard: UnitCardComponent){
  //   this.saveByUnit(this.convertUnitToSaveData(unitCard))
  // }
  // private convertUnitToSaveData(unitCardComponent: UnitCardComponent): UnitSaveData{
  //   let unitSaveData = new UnitSaveData();
  //   unitSaveData.cardId = unitCardComponent.unitCard.unit.id;
  //   unitSaveData.uuid = unitCardComponent.unitCard.unit.getUuid();
  //   unitSaveData.curHealth = unitCardComponent.unitCard.curHealth;
  //   return unitSaveData;
  // }

  private saveByUnit(unit: UnitSaveData) {
    let unitSaveData = this.saveData.unitSaves.find((unitSave) => unitSave.uuid == unit.uuid);
    if(unitSaveData != null){ // Previously exists
      let index = this.saveData.unitSaves.indexOf(unitSaveData);
      this.saveData.unitSaves[index] = unit;
      localStorage.setItem('saveData', JSON.stringify(this.saveData));
    }else{ // New Card
      this.saveData.unitSaves.push(unit);
      localStorage.setItem('saveData', JSON.stringify(this.saveData));
    }

  }
}



export interface PlayerSaveData {
  unitSaves: UnitSaveData[];
}
class UnitSaveData {
  uuid: string;
  cardId: number;
  curHealth: number;
  // stats: Stats;
}
