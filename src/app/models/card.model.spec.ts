import { TestBed } from '@angular/core/testing';
import { ArmCard, Arm, ArmsTypes, UnitCard, Item, HealItemCard, HealItem, BuffItem, BuffItemCard, HealTool, HealToolCard, Swordsman, ToolTypes, Sage, Sniper, Chef, EquippableCard, ArmsClasses, ToolClasses, ArmsClass } from './card.model';
import { Unit } from './unit.model';


describe('CardModelTests', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  // Card Creation Tests
  it('should create a weapon card with no uuid added', () => {
    let newItem = new ArmCard(getSwordWeapon());
    expect(newItem.getUuid()).toBeTruthy();
  });

  it('should create a weapon card with a specific uuid added', () => {
    let newItem = new ArmCard(getSwordWeapon(), 'xxx');
    expect(newItem.getUuid()).toEqual('xxx');
  });

  it('should create a unit card', () => {
    let newUnitCard = new UnitCard(getSwordsManHealerUnit());
    expect(newUnitCard.getCurHealth()).toEqual(100);
    expect(newUnitCard.getCurHealth()).toEqual(100);
    expect(newUnitCard.getUnit().name).toEqual("p1 Jimmy Mage 0");
    expect(newUnitCard.getArmsClass().armsList[0]).toEqual(ArmsTypes.SWORD);
  });

  it('should create a unit card and equip a weapon', () => {
    let newUnitCard = new UnitCard(getSwordsManHealerUnit());
    let newWeaponCard = new ArmCard(getSwordWeapon());

    expect(newUnitCard.equipItem(newWeaponCard)).toBeTruthy();
    expect(newUnitCard.getCurWeapon().card.name).toEqual('BASIC-ASS-SWORD');
  });

  it('should create a unit card and NOT equip a WRONG weapon', () => {
    let newUnitCard = new UnitCard(getSwordsManHealerUnit());
    let newWeaponCard = new ArmCard(getMagicWeapon());

    expect(newUnitCard.equipItem(newWeaponCard)).toBeTruthy();
    expect(newUnitCard.useItemOnTarget(0, newUnitCard)).toBeFalsy();
  });

  it('should create an Item and Equip it', () => {
    let newUnitCard = new UnitCard(getSwordsManHealerUnit());
    let newItemCard = new HealItemCard(getHealthItem());

    expect(newUnitCard.equipItem(newItemCard)).toBeTruthy();
  });

  it('should create an Item, Equip it, and use it', () => {
    let newUnitCard = new UnitCard(getSwordsManHealerUnit());
    let newItemCard = new HealItemCard(getHealthItem());

    expect(newUnitCard.equipItem(newItemCard)).toBeTruthy();
    expect(newUnitCard.useItemOnTarget(0, newUnitCard)).toBeTruthy();
  });

  it('should create an Item, Equip it, and use it too many times', () => {
    let newUnitCard = new UnitCard(getSwordsManHealerUnit());
    let newItemCard = new HealItemCard(getHealthItem());

    expect(newUnitCard.equipItem(newItemCard)).toBeTruthy();
    expect(newUnitCard.useItemOnTarget(0, newUnitCard)).toBeTruthy();
    expect(newUnitCard.useItemOnTarget(0, newUnitCard)).toBeTruthy();
    expect(newUnitCard.useItemOnTarget(0, newUnitCard)).toBeTruthy();
    expect(newUnitCard.useItemOnTarget(0, newUnitCard)).toBeFalsy();

  });


  it('should create a Healing Item, Equip it, and use it too many times', () => {
    let newUnitCard = new UnitCard(getSwordsManHealerUnit());
    let newItemCard = new HealItemCard(getHealthItem());

    // Take away health from UnitCard
    newUnitCard.takeDamage(40);
    expect(newUnitCard.getCurHealth()).toEqual(60);

    // Equip a health potion
    expect(newUnitCard.equipItem(newItemCard)).toBeTruthy();
    expect(newUnitCard.getEquippableCard(0).usesLeft).toEqual(3);

    // Use Health potion 4 times and check health each time
    expect(newUnitCard.useItemOnTarget(0, newUnitCard)).toBeTruthy();
    expect(newUnitCard.getCurHealth()).toEqual(80);

    expect(newUnitCard.useItemOnTarget(newItemCard, newUnitCard)).toBeTruthy();
    expect(newUnitCard.getCurHealth()).toEqual(100);

    expect(newUnitCard.useItemOnTarget(newItemCard, newUnitCard)).toBeTruthy();
    expect(newUnitCard.getCurHealth()).toEqual(100);

    expect(newUnitCard.getEquippableCard(0).usesLeft).toEqual(0);
    expect(newUnitCard.useItemOnTarget(newItemCard, newUnitCard)).toBeFalsy();

  });

  it('should create two units to fight each other', () => {
    let p1UnitCard = new UnitCard(getSwordsManHealerUnit());
    expect(p1UnitCard.equipItem(new HealItemCard(getHealthItem()))).toBeTruthy();
    expect(p1UnitCard.equipItem(new ArmCard(getSwordWeapon()))).toBeTruthy();
    expect(p1UnitCard.getCurWeapon().card.name).toEqual('BASIC-ASS-SWORD');

    let p2UnitCard = new UnitCard(getSwordsManHealerUnit());

    // p1 attacks p2 and checks to make sure health was taken away
    expect(p1UnitCard.useItemOnTarget(1, p2UnitCard)).toBeTruthy();
    expect(p2UnitCard.getCurHealth()).toEqual(94);

    // p2 will equip a concoction and then heal itself
    expect(p2UnitCard.equipItem(new HealItemCard(getHealthItem()))).toBeTruthy();
    expect(p2UnitCard.useItemOnTarget(p2UnitCard.getEquippableCard(0), p2UnitCard)).toBeTruthy();
    expect(p2UnitCard.getCurHealth()).toEqual(100);
  });

  it('should create a buff item and buff the character stats', () => {
    let p1UnitCard = new UnitCard(getSwordsManHealerUnit());
    expect(p1UnitCard.equipItem(new BuffItemCard(getBuffItem()))).toBeTruthy();
    expect(p1UnitCard.useItemOnTarget(0, p1UnitCard)).toBeTruthy();
    expect(p1UnitCard.getBuffsForStat("attack")).toEqual(2);
    expect(p1UnitCard.getStatsWithMods().attack).toEqual(7);

    expect(p1UnitCard.equipItem(new BuffItemCard(getBuffItem()))).toBeTruthy();
    expect(p1UnitCard.useItemOnTarget(0, p1UnitCard)).toBeTruthy();
    expect(p1UnitCard.getBuffsForStat("attack")).toEqual(4);
    expect(p1UnitCard.getStatsWithMods().attack).toEqual(9);
  });

  it('should equip a buff item and a health item', () => {
    let p1UnitCard = new UnitCard(getSwordsManHealerUnit());
    expect(p1UnitCard.equipItem(new BuffItemCard(getBuffItem()))).toBeTruthy();
    expect(p1UnitCard.equipItem(new HealItemCard(getHealthItem()))).toBeTruthy();

    expect(p1UnitCard.useItemOnTarget(0, p1UnitCard)).toBeTruthy();
    expect(p1UnitCard.useItemOnTarget(1, p1UnitCard)).toBeTruthy();
  });

  it('should equip sword, buff up, then attack', () => {
    let p1UnitCard = new UnitCard(getSwordsManHealerUnit());
    let p2UnitCard = new UnitCard(getSwordsManHealerUnit());
    expect(p1UnitCard.equipItem(new BuffItemCard(getBuffItem()))).toBeTruthy();
    expect(p1UnitCard.equipItem(new ArmCard(getSwordWeapon()))).toBeTruthy();

    expect(p1UnitCard.useItemOnTarget(0, p1UnitCard)).toBeTruthy();

    expect(p1UnitCard.useItemOnTarget(1, p2UnitCard)).toBeTruthy();
    expect(p2UnitCard.getCurHealth()).toEqual(92);
  });

  it('should equip staff and heal other unit', () => {
    let p1UnitCard = new UnitCard(getSwordsManHealerUnit());
    let p2UnitCard = new UnitCard(getSwordsManHealerUnit());
    expect(p1UnitCard.equipItem(new HealToolCard(getStafTool()))).toBeTruthy();
    expect(p1UnitCard.equipItem(new ArmCard(getSwordWeapon()))).toBeTruthy();

    expect(p1UnitCard.useItemOnTarget(0, p2UnitCard)).toBeTruthy();
    expect(p2UnitCard.getCurHealth()).toEqual(100);
  });

  it('should equip staff but CAN NOT use it', () => {
    let p1UnitCard = new UnitCard(getSniperChefUnit());
    let p2UnitCard = new UnitCard(getSwordsManHealerUnit());

    expect(p1UnitCard.equipItem(new HealToolCard(getStafTool()))).toBeTruthy();
    expect(p1UnitCard.equipItem(new ArmCard(getSwordWeapon()))).toBeTruthy();

    expect(p1UnitCard.useItemOnTarget(0, p2UnitCard)).toBeFalsy();
    expect(p2UnitCard.getCurHealth()).toEqual(100);
  });

  it('should attempt to use an item that the unit cant and still have the same number of uses', () => {
    let p1UnitCard = new UnitCard(getSniperChefUnit());
    let p2UnitCard = new UnitCard(getSwordsManHealerUnit());

    expect(p1UnitCard.equipItem(new HealToolCard(getStafTool()))).toBeTruthy();
    expect(p1UnitCard.equipItem(new ArmCard(getSwordWeapon()))).toBeTruthy();

    expect(p1UnitCard.useItemOnTarget(0, p2UnitCard)).toBeFalsy();
    expect(p2UnitCard.getCurHealth()).toEqual(100);
    expect(p1UnitCard.getEquippableCard(0).usesLeft).toEqual(p1UnitCard.getEquippableCard(0).card.uses);
  });
  // End Card Creation Tests

});

function getSwordWeapon() {
  let weapon: Arm = {
    armsType: ArmsTypes.SWORD,
    id: "1",
    minRange: 0,
    maxRange: 1,
    damage: 3,
    name: 'BASIC-ASS-SWORD',
    uses: 5
  }

  return weapon;
}
function getStafTool() {
  let weapon: HealTool = {
    toolType: ToolTypes.STAFF,
    id: "1",
    minRange: 0,
    maxRange: 1,
    healAmount: 20,
    name: 'Healing Staff!',
    uses: 5
  }

  return weapon;
}
function getMagicWeapon() {
  let weapon: Arm = {
    armsType: ArmsTypes.MAGIC,
    id: "1",
    minRange: 0,
    maxRange: 3,
    damage: 20,
    name: 'CRAZY-ASS-MAGIC',
    uses: 5
  }

  return weapon;
}
function getGenericItem() {
  let item: Item = {
    name: 'Some-lame-item',
    id: "1",
    uses: 5
  }

  return item;
}
function getHealthItem() {
  let item: HealItem = {
    name: 'Concoction',
    id: "1",
    uses: 3,
    healAmount: 20
  }

  return item;
}
function getBuffItem() {
  let item: BuffItem = {
    buff:{
      statsBuffed: {
        attack: 2,
        defense: 10,
        skill: 0,
        luck: 0,
        health: 5
      },
      turnsToLast: 3
    },
    name: 'PowerPotion',
    uses: 2,
    id: "1",

  }

  return item;
}
function getSwordsManHealerUnit() {
  let unit: Unit = {
    name: "p1 Jimmy Mage 0",
    id: "1",
    stats: {
      attack: 5,
      defense: 2,
      health: 100,
      luck: 5,
      skill: 5,
    },

    armsClass: ArmsClasses.SWORDSMAN,
    toolClass: ToolClasses.SAGE
  }

  return unit;
}

function getSniperChefUnit() {
  let unit: Unit = {
    name: "Chef Domeshotz",
    id: "1",
    stats: {
      attack: 5,
      defense: 2,
      health: 100,
      luck: 5,
      skill: 5,
    },

    armsClass: ArmsClasses.SNIPER,
    toolClass: ToolClasses.CHEF
  }

  return unit;
}
