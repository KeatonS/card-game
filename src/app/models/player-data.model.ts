export interface CardSaveData{
    uuid: string;
    id: string;
}