import { Card, UnitCard, ItemCard, EquippableCard } from "./card.model";

export class GameSession {
  public games: Game[];
}

export class Game {
  public board: Board;

  constructor(board: Board) {
    this.board = board;
  }

  setActiveUnits(playerId: number, cardIds: number[]) {
    this.board.getPlayer(playerId).cardData.activeUnits;

    // for(index in cardIds)
    // new UnitCardSpace(this.board.getPlayer(playerId).cardData.barracks[cardIds[0]])

  }

  executeGame() {
    // STEP 1 - Allow players to pick active units
    // STEP 2 - Allow players to pick passive units
    // STEP 3 - Players Draw Item Cards into their hands
    // MAIN LOOP
    // STEP 1 - Player 1 Places Items on
  }
}

export class Board {
  private players: Player[] = [];

  constructor(p1: Player, p2: Player) {
    this.players = [p1, p2];
  }

  public getPlayer(index: number): Player {
    return this.players[index];
  }
}

export class UnitCardSpace {
  unitInSpot: UnitCard;
  constructor(unitInSpot: UnitCard) {
    this.unitInSpot = unitInSpot;
  }
}

export class Player {
  public shipHealth: number;
  public cardData: PlayerCards;

  constructor(shipHealth: number, cardData: PlayerCards) {
    this.shipHealth = shipHealth;
    this.cardData = cardData;
  }
}
export class PlayerCards {
  public wholeDeck: Deck;
  public deadUnits: Card[];
  public barracks: UnitCard[]; // wholeDeck filtered on unit types
  public chest: EquippableCard[];   // wholeDeck filtered on item types
  public activeUnits: UnitCardSpace[];
  public passiveUnits: UnitCardSpace[];

  constructor(wholeDeck: Deck) {
    this.wholeDeck = wholeDeck;

    this.barracks = this.wholeDeck.cards.filter(card => {
      return card instanceof UnitCard;
    }) as UnitCard[];
  }
}

export class Deck {
  cards: Card[] = [];

  constructor(cards: Card[]) {
    this.cards = cards;
  }
}
