import { Buff, BuffItem, Card, UnitCard } from './card.model';

declare enum ActionTrigger {
    BEFORE_ATTACK = 1,
    AFTER_ATTACK,
    ON_DEFENSE,
}

export class Ability {
    name: string;
    id: string;
    description: string;
    action: AbilityAction;

    constructor() { };
}

export class AbilityAction {
    trigger: ActionTrigger;

    constructor(trigger: ActionTrigger) {
        this.trigger = trigger;
    }

    execute(abilityTarget: Card) {
        switch (this.trigger) {
            case ActionTrigger.BEFORE_ATTACK:
                this.beforeAttack(abilityTarget);
                break;
            case ActionTrigger.AFTER_ATTACK:
                this.afterAttack(abilityTarget);
                break;
            case ActionTrigger.ON_DEFENSE:
                this.onDefense(abilityTarget);
                break;
        }
    };

    beforeAttack(abilityTarget: Card) { console.log("Error, method not overridden.") };
    afterAttack(abilityTarget: Card) { console.log("Error, method not overridden.") };
    onDefense(abilityTarget: Card) { console.log("Error, method not overridden.") };

}

export class SingAbility implements Ability {
    name: string = "Sing";
    id: string = "1";
    description: string = "Can use a turn to sing to another unit and give them another turn.";
    action: SingAction = new SingAction(1);
}

class SingAction extends AbilityAction {
    beforeAttack(abilityTarget: UnitCard) {
        let buff: Buff = {
            turnsToLast: 1,
            movementIncrease: 1
        }
        abilityTarget.applyBuff(buff);
    }
}   