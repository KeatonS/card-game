import { Ability } from './ability.model';
import { ArmsTypes, ArmsClass, ToolsClass, iCard } from './card.model';


export interface UnitStats {
  attack: number;
  defense: number;
  health: number;
  luck: number;
  skill: number;
}

export interface Unit extends iCard {
  stats: UnitStats;
  armsClass: number;
  toolClass: number;
  ability?: Ability;
}
