import { Unit, UnitStats } from './unit.model';
import { v4 as uuidv4 } from 'uuid';
// import { Stats } from 'fs';


export interface iCard {
  name: string;
  id: string;
  image?: string;
  description?: string;
}
export class Card {
  public uuid: string;       // unique identifier for the individual card
  public card: iCard;

  public constructor(card: iCard, uuid?: string) {
    uuid ? this.uuid = uuid : this.uuid = uuidv4();
    this.card = card;
  }

  public getUuid() {
    return this.uuid;
  }

  public setUuid(uuid) {
    this.uuid = uuid;
  }
}

export class UnitCard extends Card {
  private unit: Unit;
  private curHealth: number;
  private heldItems: EquippableCard[] = [];
  private curEquippedWeapon: ArmCard = null;
  private maxItemSlots: number = 5;
  private buffs: Buff[] = [];
  private armsClass: ArmsClass;
  private toolsClass: ToolsClass;

  constructor(unit: Unit, uuid?: string, curHealth?: number) {
    super(unit, uuid);
    this.unit = unit;
    if (unit) {
      curHealth ? this.curHealth = curHealth : this.curHealth = this.unit.stats.health;
      this.armsClass = ArmsMapping.getClass(this.unit.armsClass);
      this.toolsClass = ToolsMapping.getClass(this.unit.toolClass);
    }
  };

  public equipItem(newItem: EquippableCard): boolean {
    if (this.heldItems.length < this.maxItemSlots) {
      this.heldItems.push(newItem);
      newItem.equipToUnit(this);
      if (newItem instanceof ArmCard && !this.curEquippedWeapon) {
        if (this.canUseArm(newItem)) {
          this.setCurWeapon(newItem);
        }
      }
      return true;
    } else {
      return false;
    }
  }

  public setCurWeapon(curWeapon: ArmCard) {
    this.curEquippedWeapon = curWeapon;
  }
  public getCurWeapon() {
    return this.curEquippedWeapon;
  }

  public getUnit() {
    return this.unit;
  }

  public getCurHealth() {
    return this.curHealth;
  }

  public takeDamage(damage: number) {
    this.curHealth = this.curHealth - damage;
    if (this.curHealth < 0) this.curHealth = 0;
    else if (this.curHealth > this.unit.stats.health) this.curHealth = this.unit.stats.health;
  }

  public restoreHealth(healAmount: number) {
    this.curHealth = this.curHealth + healAmount;
    if (this.curHealth < 0) this.curHealth = 0;
    else if (this.curHealth > this.unit.stats.health) this.curHealth = this.unit.stats.health;

  }

  public useItemOnTarget(item: number | EquippableCard, target: UnitCard): boolean {
    if (item instanceof EquippableCard) {
      return item.useOnTarget(target);
    } else if (typeof item === "number") {
      let retVal = this.getEquippableCard(item).useOnTarget(target);
      return retVal;
    }
  }

  public getEquippableCard(index: number): EquippableCard {
    if (index >= 0 && index < this.heldItems.length) {
      return this.heldItems[index];
    } else {
      console.error('Trying to acces an item out of bounds');
      return null;
    }
  }

  public applyBuff(buff: Buff) {
    this.buffs.push(buff);
  }

  public getStatsWithMods(): UnitStats {
    let modifiedStats: UnitStats = {
      attack: this.unit.stats.attack + this.getBuffsForStat("attack"),
      defense: this.unit.stats.defense + this.getBuffsForStat("defense"),
      skill: this.unit.stats.skill + this.getBuffsForStat("skill"),
      luck: this.unit.stats.luck + this.getBuffsForStat("luck"),
      health: this.unit.stats.health + this.getBuffsForStat("health"),
    }

    return modifiedStats;
  }

  public getBuffsForStat(stat: string): number {
    let buffAcc = 0;
    this.buffs.forEach(buff => {
      buffAcc += buff.statsBuffed[stat];
    });

    return buffAcc;
  }

  public getAllBuffs(): Buff {
    let returnBuff: Buff;

    this.buffs.forEach(buff => {
      returnBuff = addBuffs(returnBuff, buff);
    });

    return returnBuff;
  }

  public canUseArm(weapon: ArmCard): boolean {
    return this.armsClass.armsList.includes(weapon.card.armsType);
  }

  public canUseTool(tool: ToolCard): boolean {
    return this.toolsClass.toolsList.includes(tool.card.toolType);
  }

  public getArmsClass() {
    return this.armsClass;
  }

  public getToolsClass() {
    return this.toolsClass;
  }
}

export abstract class EquippableCard extends Card {
  card: Equippable;
  usesLeft: number;
  owner: UnitCard;

  constructor(equipment: Equippable, uuid?) {
    super(equipment, uuid);
    if (equipment) this.usesLeft = equipment.uses;
  }

  hasUsesLeft(): boolean {
    return this.usesLeft > 0;
  }


  equipToUnit(unitCard: UnitCard) {
    this.owner = unitCard;
  }

  unequipFromUnit() {
    this.owner = null;
  }

  useOnTarget(targetUnit: UnitCard): boolean {
    throw new Error("Method not implemented.");
  }
  canUseItem(): boolean {
    throw new Error("Method not implemented.");
  }
}

export interface Equippable extends iCard {
  uses: number;
  description?: string;
}

export interface Item extends Equippable {
}

export abstract class ItemCard extends EquippableCard {
}

export class ArmOrToolCard extends EquippableCard {
  constructor(card, uuid) {
    super(card, uuid);
  }
}

export interface HealItem extends Item {
  healAmount: number;
}

export interface BuffItem extends Item {
  buff: Buff;
}

export interface Buff {
  statsBuffed?: UnitStats;
  movementIncrease?: number;
  turnsToLast: number;
}

export function addBuffs(buff1: Buff, buff2: Buff): Buff {
  let statsSum: UnitStats = {
    attack: buff1.statsBuffed.attack + buff2.statsBuffed.attack,
    defense: buff1.statsBuffed.defense + buff2.statsBuffed.defense,
    health: buff1.statsBuffed.health + buff2.statsBuffed.health,
    luck: buff1.statsBuffed.luck + buff2.statsBuffed.luck,
    skill: buff1.statsBuffed.attack + buff2.statsBuffed.skill
  };

  let retBuff: Buff = {
    statsBuffed: statsSum,
    turnsToLast: -1,
    movementIncrease: buff1.movementIncrease + buff2.movementIncrease
  }

  return retBuff;
}
export class HealItemCard extends ItemCard {

  constructor(item: HealItem, uuid?: string) {
    super(item, uuid);
  }
  useItem(): boolean {
    if (this.hasUsesLeft()) {
      this.owner.restoreHealth((this.card as HealItem).healAmount);
      return true;
    } else {
      return false;
    }
  }

  useOnTarget(target: UnitCard): boolean {
    if (super.hasUsesLeft()) {
      this.usesLeft--;
      target.restoreHealth(this.card["healAmount"]);
      return true;
    } else {
      return false;
    }
  }
}

export class BuffItemCard extends ItemCard {

  equipment: BuffItem;

  constructor(equipment: BuffItem, uuid?: string) {
    super(equipment, uuid);
    this.equipment = equipment;
  }

  useOnTarget(target: UnitCard): boolean {
    if (super.hasUsesLeft()) {
      this.usesLeft--;
      target.applyBuff(<Buff>this.equipment.buff);
      return true;
    } else {
      return false;
    }
  }

}

////////////////////////// ARMS ///////////////////////////////

export enum ArmsClasses {
  SWORDSMAN,
  SNIPER,
  MAGE,
  BUCCANEER,
  BERSERKER,
  DARKMAGE,
}

export class ArmsClass {
  className: string;
  armsList: ArmsTypes[];
  baseStats?: UnitStats;
}

export class Swordsman extends ArmsClass {
  armsList = [ArmsTypes.SWORD];
  className = "Swordsman";
}

export class Buccaneer extends ArmsClass {
  armsList = [ArmsTypes.SWORD, ArmsTypes.PISTOL];
  className = "Buccaneer";
}
export class Mage extends ArmsClass {
  armsList = [ArmsTypes.MAGIC];
  className = "Mage";
}
export class Sniper extends ArmsClass {
  armsList = [ArmsTypes.PISTOL, ArmsTypes.RIFLE];
  className = "Sniper";
}
export class Berserker extends ArmsClass {
  armsList = [ArmsTypes.AXE];
  className = "Berserker";
}
export class DarkMage extends ArmsClass {
  armsList = [ArmsTypes.DARKMAGIC];
  className = "Dark Mage";
}
export enum ArmsTypes {
  SWORD,
  RIFLE,
  PISTOL,
  MAGIC,
  DARKMAGIC,
  AXE,
  FISH
}

export class ArmsMapping {
  public static map = new Map([
    [ArmsClasses.MAGE, new Mage()],
    [ArmsClasses.SWORDSMAN, new Swordsman()],
    [ArmsClasses.SNIPER, new Sniper()],
    [ArmsClasses.BUCCANEER, new Buccaneer()],
    [ArmsClasses.BERSERKER, new Berserker()],
    [ArmsClasses.DARKMAGE, new DarkMage()],

  ]);

  public static getClass(key: number) {
    return this.map.get(key);
  }
}
////////////////////////// TOOOOOLS ///////////////////////////////

export enum ToolClasses {
  CHEF,
  DOCTOR,
  SAGE,
  MUSICIAN,
  CARPENTER,
  DECKHAND,
  NAVIGATOR,
  FISHMAN
}
export interface ToolsClass {
  className: string;
  toolsList: ToolTypes[];
  baseStats?: UnitStats;
}
export class Sage implements ToolsClass {
  toolsList = [ToolTypes.STAFF];
  className = "Sage";
}
export class Doctor implements ToolsClass {
  toolsList = [ToolTypes.MEDICAL];
  className = "Doctor";
}
export class Chef implements ToolsClass {
  toolsList = [ToolTypes.COOKING];
  className = "Chef";
}
export class Musician implements ToolsClass {
  toolsList = [ToolTypes.INSTRUMENTS];
  className = "Musician";
}
export class Carpenter implements ToolsClass {
  toolsList = [ToolTypes.CARPENTRY];
  className = "Carpenter";
}
export class Deckhand implements ToolsClass {
  toolsList = [ToolTypes.DECKHAND];
  className = "Deckhand";
}
export class Navigator implements ToolsClass {
  toolsList = [ToolTypes.NAVIGATION];
  className = "Navigator";
}
export class Fishman implements ToolsClass {
  toolsList = [ToolTypes.FISHMAN];
  className = "Fishman";
}
export enum ToolTypes {
  MEDICAL,
  CARPENTRY,
  COOKING,
  NAVIGATION,
  STAFF,
  INSTRUMENTS,
  DECKHAND,
  FISHMAN
}

export class ToolsMapping {
  public static map = new Map([
    [ToolClasses.CHEF, new Chef()],
    [ToolClasses.DOCTOR, new Doctor()],
    [ToolClasses.SAGE, new Sage()],
    [ToolClasses.CARPENTER, new Carpenter()],
    [ToolClasses.MUSICIAN, new Musician()],
    [ToolClasses.DECKHAND, new Deckhand()],
    [ToolClasses.NAVIGATOR, new Navigator()],
    [ToolClasses.FISHMAN, new Fishman()],

  ]);

  public static getClass(key: number) {
    return this.map.get(key);
  }
}
export interface Arm extends Equippable {
  armsType: ArmsTypes;
  maxRange: number;
  minRange: number;
  damage: number;
}
export interface Tool extends Equippable {
  toolType: ToolTypes;
  maxRange: number;
  minRange: number;
}

export interface HealTool extends Tool {
  healAmount: number;
}

export class ToolCard extends ArmOrToolCard {
  card: Tool;

  constructor(tool: Tool, uuid?: string) {
    super(tool, uuid);
  }
  canUseItem(): boolean {
    return this.hasUsesLeft() && this.owner.canUseTool(this);
  }

  useOnTarget(targetUnit: UnitCard): boolean {
    console.error("Trying to execute generic method for ToolCard Class. Please override in derived class");
    return false;
  }
}

export interface TargetableCard {
  useOnTarget(targetUnit: UnitCard): boolean;

  canUseItem(): boolean;
}

export class HealToolCard extends ToolCard {
  card: HealTool;

  constructor(tool: HealTool, uuid?: string) {
    super(tool, uuid);
  }

  useOnTarget(targetUnit: UnitCard): boolean {
    if (this.canUseItem()) {
      this.usesLeft--;
      targetUnit.restoreHealth(this.card.healAmount);
      return true;
    } else {
      return false;
    }
  }
}
export class ArmCard extends ArmOrToolCard {

  card: Arm;         // particular item from items.json

  constructor(weapon: Arm, uuid?: string) {
    super(weapon, uuid);
  }

  calculateDamageToEnemy(targetUnit: UnitCard) {
    return this.getAttackPower() - targetUnit.getStatsWithMods().defense;
  }
  getAttackPower(): number {
    return this.card.damage + this.owner.getStatsWithMods().attack;
  }

  canUseItem(): boolean {
    return this.hasUsesLeft() && this.owner.canUseArm(this);
  }

  useOnTarget(targetUnit: UnitCard): boolean {
    if (this.canUseItem()) {
      this.usesLeft--;
      targetUnit.takeDamage(this.calculateDamageToEnemy(targetUnit));
      return true;
    } else {
      return false;
    }
  }

}
