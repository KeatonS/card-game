import { TestBed } from "@angular/core/testing";
import { SingAbility } from './ability.model';
import { ArmsClasses, ToolClasses, UnitCard } from './card.model';
import { Unit } from './unit.model';

describe('AbilityModelTests', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should create a musician and give a unit another turn', () => {
        // let musician = new UnitCard(getMusicianUnit());
        // let swordsman = new UnitCard(getSwordsManHealerUnit());

        // // expect(musician.getUnit().ability.name).toEqual("Butt");
        // musician.getUnit().ability.action.execute(swordsman);
        // let buffedMovementCount = swordsman.getAllBuffs().movementIncrease;
        // expect(buffedMovementCount).toEqual(1);
        expect(true).toEqual(true);
    });

});

function getMusicianUnit() {
    let unit: Unit = {
        name: "Brook",
        id: "69",
        stats: {
            attack: 5,
            defense: 2,
            health: 100,
            luck: 5,
            skill: 5,
        },

        armsClass: ArmsClasses.SWORDSMAN,
        toolClass: ToolClasses.MUSICIAN,
        ability: new SingAbility(),
    }

    return unit;
}

function getSwordsManHealerUnit() {
    let unit: Unit = {
        name: "p1 Jimmy Mage 0",
        id: "1",
        stats: {
            attack: 5,
            defense: 2,
            health: 100,
            luck: 5,
            skill: 5,
        },

        armsClass: ArmsClasses.SWORDSMAN,
        toolClass: ToolClasses.SAGE
    }

    return unit;
}
