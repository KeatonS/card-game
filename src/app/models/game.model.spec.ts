// This is a MAJOR integration test, since the Game Model is using ALL Services and creating demo test games
import { TestBed } from '@angular/core/testing';
import { CardDataService } from '../services/card-data.service';
import { GameSession, Game, Board, Deck, Player, PlayerCards } from './game.model';
import { CardSaveData } from './player-data.model';
import PlayerOneJson from '../../assets/player-data/player-one-data.json'
import PlayerTwoJson from '../../assets/player-data/player-two-data.json'
import { ArmCard, EquippableCard, ToolCard, UnitCard } from './card.model';

describe('GameModelTests', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const gameSession = new GameSession();
    expect(gameSession).toBeTruthy();
  });

  // Testing functionality for putting units on the board and attacking and moving

  // Setting up a basic, 3 array board
  it('should set up a game board', () => {
    const gameSession = new GameSession();
    expect(gameSession).toBeTruthy();
    const playerDataService: CardDataService = TestBed.get(CardDataService);

    let game: Game = new Game(playerDataService.getBoard(PlayerOneJson, PlayerTwoJson));

    expect(game.board.getPlayer(0).cardData.wholeDeck.cards[0].getUuid()).toEqual('aaa');
    expect(game.board.getPlayer(0).cardData.wholeDeck.cards[1].getUuid()).toEqual('bbb');
    expect((game.board.getPlayer(0).cardData.wholeDeck.cards[0] as UnitCard).getUnit().name).toEqual('Gaius');
    expect((game.board.getPlayer(0).cardData.wholeDeck.cards[1] as UnitCard).getUnit().name).toEqual('Brook');
    expect((game.board.getPlayer(0).cardData.wholeDeck.cards[6] as ToolCard).card.name).toEqual('Healing Staff');
    expect((game.board.getPlayer(0).cardData.wholeDeck.cards[6] as ToolCard).uuid).toEqual('ggg');
    expect((game.board.getPlayer(0).cardData.wholeDeck.cards[7] as ArmCard).uuid).toEqual('hhh');
    expect((game.board.getPlayer(0).cardData.wholeDeck.cards[0] as UnitCard).equipItem((game.board.getPlayer(0).cardData.wholeDeck.cards[6] as ToolCard))).toBeTruthy();
    expect((game.board.getPlayer(0).cardData.wholeDeck.cards[0] as UnitCard).useItemOnTarget(0, (game.board.getPlayer(0).cardData.wholeDeck.cards[1] as UnitCard))).toBeFalsy();

    expect(game.board.getPlayer(1).cardData.wholeDeck.cards[0].getUuid()).toEqual('zzz');
    expect(game.board.getPlayer(1).cardData.wholeDeck.cards[1].getUuid()).toEqual('yyy');

    expect(game.board.getPlayer(0).cardData.barracks[0].card.name).toEqual('Bucky');
    expect(game.board.getPlayer(0).cardData.barracks.length).toEqual(6);

    expect(game.board.getPlayer(1).cardData.barracks[0].card.name).toEqual('Donnel');
    expect(game.board.getPlayer(1).cardData.barracks.length).toEqual(6);



  });
});
