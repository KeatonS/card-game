import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UnitCardComponent } from './components/unit-card/unit-card.component';
import { GameStateMachineService } from './services/game-manager.service';
import { GameSaveService } from './services/game-save.service';
import { HttpClientModule } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CardDeckComponent } from './components/card-deck/card-deck.component';
import { CardSpotComponent } from './components/card-spot/card-spot.component';
import { GameBoardComponent } from './components/game-board/game-board.component';
import { CardComponent } from './components/card/card.component';
import { ActiveCardComponent } from './components/active-card/active-card.component';
import { CommonModule } from '@angular/common';
import { DeckBuilderComponent } from './components/deck-builder/deck-builder.component';
import { CardListItemComponent } from './components/card-list-item/card-list-item.component';
import { CardNameSearchPipe } from './pipes/card-name-search.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    UnitCardComponent,
    CardDeckComponent,
    CardSpotComponent,
    GameBoardComponent,
    CardComponent,
    ActiveCardComponent,
    DeckBuilderComponent,
    CardListItemComponent,
    CardNameSearchPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DragDropModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [GameSaveService, GameStateMachineService],
  bootstrap: [AppComponent]
})
export class AppModule { }
