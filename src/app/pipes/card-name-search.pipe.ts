import { Pipe, PipeTransform } from '@angular/core';
import { Card } from '../models/card.model';
@Pipe({
  name: 'filter'
})
export class CardNameSearchPipe implements PipeTransform {
  transform(items: Card[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;

    searchText = searchText.toLowerCase();
    return items.filter(it => {
      return it.card.name.toLowerCase().includes(searchText);
    });
  }
}
