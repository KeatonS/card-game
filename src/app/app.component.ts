import { Component } from '@angular/core';
import { GameStateMachineService } from 'src/app/services/game-manager.service';
import { GameSaveService, PlayerSaveData } from 'src/app/services/game-save.service';
import { Unit } from 'src/app/models/unit.model';
import { Card } from './models/card.model';
import { stringify } from 'querystring';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'game-app';

  public activePlayerCard: Card;
  public saveCards: PlayerSaveData;

  player1UnitCards: MyCard[];
  player2UnitCards: MyCard[];
  x: WeaponCard;
  y: WeaponCard;
  z: WeaponCard;
  staff: WeaponCard;
  playerToGo: number = 0;

  constructor(private gameManagerService: GameStateMachineService,
    private gameSaveService: GameSaveService) {

    this.gameManagerService.getActivePlayerCard().subscribe(data => {
      this.activePlayerCard = data;
    });

    this.gameSaveService.getPlayerSaveDataSubject().subscribe(data => {
      this.saveCards = data;
    });

    this.initializeWeapons();
    this.initializeDecks();
    // this.sampleFight();
  }

  initializeWeapons() {
    this.x = new WeaponCard();
    this.x.weapon = {
      weaponClass: WeaponClass.SWORD,
      minRange: 0,
      maxRange: 1,
      damage: 3,
      name: 'BASIC-ASS-SWORD'
    }
    this.y = new WeaponCard();
    this.y.weapon = {
      weaponClass: WeaponClass.MAGIC,
      minRange: 0,
      maxRange: 3,
      damage: 20,
      name: 'CRAZY-ASS-MAGIC'
    }
    this.z = new WeaponCard();
    this.z.weapon = {
      weaponClass: WeaponClass.GUN,
      minRange: 0,
      maxRange: 3,
      damage: 20,
      name: 'A FUCKING RIFLE'
    }
    this.staff = new WeaponCard();
    this.staff.weapon = {
      weaponClass: WeaponClass.STAFF,
      minRange: 0,
      maxRange: 3,
      damage: -10,
      name: 'A LAMEASS STAFF'
    }
  }
  initializeDecks() {
    // Making player 1's deck
    this.player1UnitCards = [
      new MyCard({
        name: "p1 Jimmy Mage 0",
        attack: 5,
        health: 100,
        luck: 5,
        skill: 5,
        weaponClasses: [WeaponClass.MAGIC]
      }, "101"),
      new MyCard({
        name: "p1 Johnny Sniper 1",
        attack: 5,
        health: 100,
        luck: 5,
        skill: 5,
        weaponClasses: [WeaponClass.GUN]
      }, "102"),
      new MyCard({
        name: "p1 Jerry Sage 2",
        attack: 5,
        health: 100,
        luck: 5,
        skill: 5,
        weaponClasses: [WeaponClass.STAFF]
      }, "103"),
    ];

    // Making player 2's deck
    this.player2UnitCards = [
      new MyCard({
        name: "p2 Tommy Mage 0",
        attack: 5,
        health: 100,
        luck: 5,
        skill: 5,
        weaponClasses: [WeaponClass.MAGIC]
      }, "201"),
      new MyCard({
        name: "p2 Tony Sniper 1",
        attack: 5,
        health: 100,
        luck: 5,
        skill: 5,
        weaponClasses: [WeaponClass.GUN]
      }, "202"),
      new MyCard({
        name: "p2 Terry Sage 2",
        attack: 5,
        health: 100,
        luck: 5,
        skill: 5,
        weaponClasses: [WeaponClass.STAFF]
      }, "203"),
    ];
  }
  // MAIN EXECUTION RIGHT HERE BABIEEE
  sampleFight() {
    // Equiping Items
    for (let i = 0; i < 3; i++) {
      this.player1UnitCards[i].equipWeapon([this.x, this.y, this.z, this.staff]);
      this.player2UnitCards[i].equipWeapon([this.x, this.y, this.z, this.staff]);
      this.player1UnitCards[i].curItem = 0;
      this.player2UnitCards[i].curItem = 0;
    }

    let gameOver = false;
    let turnCount = 1;
    let playerCount = 2;


    console.log("**********************************");

    let player1: Player = new Player("Keaton", this.player1UnitCards, 100);
    let player2: Player = new Player("Alivia", this.player2UnitCards, 100);
    let playerArry: Player[] = [player1, player2];

    for (let i = 0; !gameOver; i++) { // Main Game loop
      this.playerToGo = (i % playerCount) + 1;
      let defendingPlayerIndex = (this.playerToGo == 1) ? 1 : 0;

      console.log("*********** Turn: " + turnCount + " **************************");
      console.log("Player " + playerArry[this.playerToGo - 1].name + "'s Turn");

      let alreadyAttackedList = [];

      for (let j = 0; j < playerArry[this.playerToGo - 1].activeCards.length; j++) {
        if (playerArry[this.playerToGo - 1].activeCards[j].curHealth <= 0) {
          continue;
        }

        // One char attacks per turn
        let attackerIndex = Math.floor(Math.random() * 3);
        while (alreadyAttackedList.includes(attackerIndex)) {
          attackerIndex = Math.floor(Math.random() * 3);
        }
        let attacker = playerArry[this.playerToGo - 1].activeCards[attackerIndex];
        console.log("--" + attacker.myUnit.name + " is taking its turn. --");


        // If Staff, target a teammate
        if (attacker.getCurWeapon().weapon.weaponClass == WeaponClass.STAFF) {
          let targetIndex = this.getHealingTarget(playerArry[this.playerToGo - 1].activeCards, attackerIndex);
          this.healManager(attacker, playerArry[this.playerToGo - 1].activeCards[targetIndex]);
        } else {
          let defenderIndex = this.getEnemyTarget(playerArry, attackerIndex, defendingPlayerIndex);
          if (defenderIndex > -1) this.fightManager(playerArry[this.playerToGo - 1].activeCards[attackerIndex], playerArry[defendingPlayerIndex].activeCards[defenderIndex], Math.abs(attackerIndex - defenderIndex));
          console.log('Attacker: ' + attackerIndex + ' | Defender: ' + defenderIndex + ' | Dist: ' + Math.abs(attackerIndex - defenderIndex));
        }
      }

      this.printBoard(playerArry);

      // Is the game over?
      // this.printBoard(playerArry);
      let loserIndex = this.checkGameOver(playerArry);
      if (loserIndex != null) console.log(playerArry[loserIndex].name + " lost the game in " + turnCount + " turns !!");
      turnCount += 1;
      if (turnCount > 30 || loserIndex != null) gameOver = true;
    }

  }

  players = 2;
  activePlayer = 1;
  turnManager(action, args) {
    action(args);
  }

  healManager(healer: MyCard, healTarget: MyCard){
    console.log(healer.myUnit.name + " heals " + healTarget.myUnit.name + " with a " + healer.getCurWeapon().weapon.name + "!");

  }

  isInRange(attacker: MyCard, distance: number): boolean {
    // Check if defender is out of range
    let tooFar = distance + 1 > attacker.heldItems[attacker.curItem].weapon.maxRange;
    let tooClose = distance + 1 < attacker.heldItems[attacker.curItem].weapon.minRange;
    let outOfRange = tooClose || tooFar;
    if (outOfRange) {
      // if (tooFar) {
      //   console.log(defender.myUnit.name + ' is too FAR for ' + attacker.myUnit.name + "'s attack");
      // } else if (tooClose) {
      //   console.log(defender.myUnit.name + ' is too CLOSE for ' + attacker.myUnit.name + "'s attack");
      // }
      return false;
    } else {
      return true;
    }
  }

  getHealingTarget(playerArry: MyCard[], attackerIndex): number {
    // Can't heal yourself and they have to be in range
    let teamArry = playerArry.filter((value, arrIndex) => {
      let inRange =  this.isInRange(playerArry[attackerIndex], Math.abs(arrIndex - attackerIndex));
      if(attackerIndex !== arrIndex && inRange){
        return attackerIndex;
      }
    });

    // Get the player with the lowest health
    let healed = teamArry.reduce(function (weakest, card) {
      return (weakest.curHealth || 0) > card.curHealth ? weakest : card;
    })

    console.log("The weakest is " + healed.myUnit.name + " with health of " + healed.curHealth);

    return playerArry.indexOf(healed);
  }

  getEnemyTarget(playerArry, attackerIndex, defendingPlayerIndex): number {
    // See which enemies are dead
    let alreadyDeadEnemies: number[] = [];
    for (let x = 0; x < playerArry[defendingPlayerIndex].activeCards.length; x++) {
      if (playerArry[defendingPlayerIndex].activeCards[x].curHealth <= 0) alreadyDeadEnemies.push(x);
    }

    // See which enemies are in range
    let enemiesInRange: number[] = [];
    for (let x = 0; x < playerArry[defendingPlayerIndex].activeCards.length; x++) {
      let inRange = this.isInRange(playerArry[this.playerToGo - 1].activeCards[attackerIndex], Math.abs(attackerIndex - x));
      if (inRange) enemiesInRange.push(x);
    }
    if (enemiesInRange.length == 0) {
      console.log("No enemies are in range.");
    } else {
      console.log("Enemy indices in range: " + enemiesInRange);
    }
    console.log("Dead enemy indices: " + alreadyDeadEnemies);

    // See which enemies are in range and not dead
    let allRemainingEnemies: number[] = [];
    enemiesInRange.forEach(enemyInRange => {
      if (!alreadyDeadEnemies.includes(enemiesInRange.indexOf(enemyInRange))) {
        allRemainingEnemies.push(enemyInRange);
      }
    });
    console.log("Enemies able to hit: " + allRemainingEnemies);

    let defenderIndex = -1;

    if (allRemainingEnemies.length > 0) {
      defenderIndex = Math.floor(Math.random() * 3);
      let iter = 0;
      while (!allRemainingEnemies.includes(defenderIndex) && iter < 10 && alreadyDeadEnemies.length < playerArry[defendingPlayerIndex].activeCards.length) {
        defenderIndex = Math.floor(Math.random() * 3);
        iter++;
      }
      if (iter == 10) {
        console.log("No enemies to attack");
        return -1;
      }

      return defenderIndex

    } else {
      console.log(playerArry[this.playerToGo - 1].activeCards[attackerIndex].myUnit.name + ` had to skip it's turn [${attackerIndex}]`);
      return -1;
    }
  }

  fightManager(attacker: MyCard, defender: MyCard, distance: number) {
    console.log(attacker.myUnit.name + " attacks " + defender.myUnit.name + " with a " + attacker.heldItems[attacker.curItem].weapon.name + "!");
    // Check if defender is not already dead 
    if (defender.curHealth <= 0) {
      console.log(defender.myUnit.name + " is already dead.");
      return 0;
    }

    let inRange = this.isInRange(attacker, distance);
    if (!inRange) return 0;

    // Determine if it is a critical hit
    let critDiff = attacker.myUnit.skill - defender.myUnit.luck;
    let critHappens: boolean = false;
    if (critDiff > 0) {
      let critChance = critDiff * 10;
      let res = Math.floor(Math.random() * (100));
      if (res <= critChance) {
        critHappens = true;
      }
    }

    // Apply damage
    let damage = attacker.heldItems[attacker.curItem].weapon.damage + attacker.myUnit.attack;
    critHappens ? damage = damage * 3 : '';
    if (critHappens) console.log(attacker.myUnit.name + " lands a critical hit!");
    console.log(defender.myUnit.name + " takes " + damage + " damage.");

    if (damage > defender.curHealth) damage = defender.curHealth; // dont go into negative health
    defender.curHealth = defender.curHealth - damage;

    // See if dead or still living
    if (defender.curHealth <= 0) {
      console.log(defender.myUnit.name + " has fainted!");
    } else {
      console.log(defender.myUnit.name + " has " + defender.curHealth + " HP left.");
    }

    return;
  }

  gameStatus(cards: MyCard[]) {
    console.log("**********");
    cards.forEach((card) => {
      console.log(card);
    });
  }

  checkGameOver(players: Player[]): number {
    let loserIndex = null;

    for (let i = 0; i < players.length; i++) {
      let gameOver = false;
      let totalHealth = 0;
      players[i].activeCards.forEach(card => {
        totalHealth += card.curHealth;
      })
      if (totalHealth <= 0) {
        gameOver = true;
      }

      if (gameOver) {
        loserIndex = i;
      }
    }
    return loserIndex;
  }

  printBoard(players: Player[]) {
    for (let i = 0; i < players.length; i++) {
      let msg: string = `(P${i + 1}):`;
      players[i].activeCards.forEach(card => {
        msg += ` [${card.curHealth}] `;
      })
      console.log(msg);
    }
  }
}

interface MyUnit {
  name: string;
  attack: number;
  health: number;
  luck: number;
  skill: number;
  weaponClasses: WeaponClass[];
}

class Item {
  thing: number;
  dosomething() { };
}
enum WeaponClass {
  SWORD,
  GUN,
  MAGIC,
  STAFF
}
interface Weapon {
  weaponClass: WeaponClass;
  maxRange: number;
  minRange: number;
  damage: number;
  name: string;
}
class ItemCard {
}
class WeaponCard extends ItemCard {
  public weapon: Weapon;
  public uuid: string;
}

class MyCard {
  public myUnit: MyUnit;
  public uuid: string;
  public curHealth: number;
  public heldItems: WeaponCard[] = [];
  public curItem: number = null;

  constructor(myUnit: MyUnit, uuid: string, curHealth?: number) {
    this.myUnit = myUnit;
    this.uuid = uuid;
    if (curHealth) {
      this.curHealth = curHealth;
    } else {
      this.curHealth = this.myUnit.health;
    }
  };

  public equipWeapon(newWeapons: WeaponCard[]) {
    newWeapons.forEach(newWeapon => {
      if (this.myUnit.weaponClasses.includes(newWeapon.weapon.weaponClass)) {
        console.log(this.myUnit.name + " equipped " + newWeapon.weapon.name)
        this.heldItems.push(newWeapon);
      } else {
        console.log(this.myUnit.name + " can not equip " + newWeapon.weapon.name + " as a weapon.")
      }
    });
  }

  public getCurWeapon() {
    return this.heldItems[this.curItem];
  }
}

class Player {
  public name: string;
  public activeCards: MyCard[];
  public health: number;
  public deck: MyCard[];

  constructor(name: string, activeCards: MyCard[], health: number, deck?: MyCard[]) {
    this.name = name;
    this.activeCards = activeCards;
    this.health = health;
    (deck) ? this.deck = deck : this.deck = this.activeCards;
  }
}
