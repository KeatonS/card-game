import { ArmsClasses, ToolClasses, ToolTypes, UnitCard } from "src/app/models/card.model";
import { CardSaveData } from "src/app/models/player-data.model";
import { Unit } from "src/app/models/unit.model";

export function getUnit(saveData: CardSaveData): UnitCard {
  switch (saveData.id.substr(2, 3)) {
    case ('001'): {
      let unit: Unit = {
        name: "Gaius",
        id: "01001",
        image: '/assets/images/units/Gaia.png',
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.BUCCANEER,
        toolClass: null
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('002'): {
      let unit: Unit = {
        name: "Brook",
        image: '/assets/images/units/brook.jpg',
        id: "01002",
        stats: {
          attack: 5,
          defense: 2,
          health: 60,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.SWORDSMAN,
        toolClass: ToolClasses.MUSICIAN
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('003'): {
      let unit: Unit = {
        name: "Willoughby",
        id: "01003",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.SNIPER,
        toolClass: ToolClasses.DOCTOR
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('004'): {
      let unit: Unit = {
        name: "Gibby",
        id: "01004",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: null,
        toolClass: ToolTypes.FISHMAN
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('005'): {
      let unit: Unit = {
        name: "Zoro",
        id: "01005",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.SWORDSMAN,
        toolClass: null
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('006'): {
      let unit: Unit = {
        name: "Usopp",
        id: "01006",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.SNIPER,
        toolClass: ToolClasses.CARPENTER
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('007'): {
      let unit: Unit = {
        name: "Donnel",
        id: "01007",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: null,
        toolClass: ToolClasses.DECKHAND
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('008'): {
      let unit: Unit = {
        name: "Vaike",
        image: '/assets/images/units/Vake.png',
        id: "01008",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.BERSERKER,
        toolClass: ToolClasses.CARPENTER
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('009'): {
      let unit: Unit = {
        name: "Shirley",
        id: "01009",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.MAGE,
        toolClass: ToolClasses.SAGE
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('010'): {
      let unit: Unit = {
        name: "Nami",
        id: "01010",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: null,
        toolClass: ToolClasses.NAVIGATOR
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('011'): {
      let unit: Unit = {
        name: "Henry",
        image: '/assets/images/units/Henry.png',
        id: "01011",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: ArmsClasses.DARKMAGE,
        toolClass: null
      }
      return new UnitCard(unit, saveData.uuid);
    }
    case ('012'): {
      let unit: Unit = {
        name: "Sanji",
        id: "01012",
        stats: {
          attack: 5,
          defense: 2,
          health: 100,
          luck: 5,
          skill: 5,
        },
        armsClass: null,
        toolClass: ToolClasses.CHEF
      }
      return new UnitCard(unit, saveData.uuid);
    }
  }
  return new UnitCard(null, '-1');
}
