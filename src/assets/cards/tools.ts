import { HealTool, HealToolCard, Tool, ToolCard, ToolClasses, ToolTypes } from "src/app/models/card.model";
import { CardSaveData } from "src/app/models/player-data.model";

export function getTool(saveData: CardSaveData): ToolCard {
  switch (saveData.id.substr(2, 3)) {
    case ('001'): {
      let tool: HealTool = {
        name: "Healing Staff",
        image: '/assets/images/tools/Healing_Staff.png',
        id: "02001",
        toolType: ToolTypes.STAFF,
        uses: 5,
        minRange: 0,
        maxRange: 2,
        healAmount: 20,
        description: 'A common staff used by sages for healing allies.'
      }
      return new HealToolCard(tool, saveData.uuid);
    }
    case ('002'): {
      let tool: Tool = {
        name: "Drums",
        id: "02002",
        toolType: ToolTypes.INSTRUMENTS,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('003'): {
      let tool: Tool = {
        name: "Guitar",
        id: "02003",
        toolType: ToolTypes.INSTRUMENTS,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('004'): {
      let tool: Tool = {
        name: "Hammer",
        id: "02004",
        toolType: ToolTypes.CARPENTRY,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('005'): {
      let tool: Tool = {
        name: "Deep Sea Mine",
        id: "02004",
        toolType: ToolTypes.FISHMAN,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('006'): {
      let tool: Tool = {
        name: "Red Syringe",
        id: "02006",
        toolType: ToolTypes.MEDICAL,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('008'): {
      let tool: Tool = {
        name: "Tourniquet",
        id: "02008",
        toolType: ToolTypes.MEDICAL,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('009'): {
      let tool: Tool = {
        name: "Board and Nails",
        id: "02009",
        toolType: ToolTypes.CARPENTRY,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('010'): {
      let tool: Tool = {
        name: "Frying Pan",
        id: "02010",
        toolType: ToolTypes.COOKING,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('011'): {
      let tool: Tool = {
        name: "Rusty Violin",
        id: "02011",
        toolType: ToolTypes.INSTRUMENTS,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('012'): {
      let tool: Tool = {
        name: "Wind Vein",
        id: "02012",
        toolType: ToolTypes.NAVIGATION,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('013'): {
      let tool: Tool = {
        name: "Telescope",
        id: "02013",
        toolType: ToolTypes.NAVIGATION,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('014'): {
      let tool: Tool = {
        name: "Super Scope",
        id: "02014",
        toolType: ToolTypes.NAVIGATION,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('015'): {
      let tool: Tool = {
        name: "Astrolabe",
        id: "02015",
        toolType: ToolTypes.NAVIGATION,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
    case ('016'): {
      let tool: Tool = {
        name: "Fishnet",
        id: "02016",
        toolType: ToolTypes.CARPENTRY,
        uses: 5,
        minRange: 0,
        maxRange: 1,
      }
      return new ToolCard(tool, saveData.uuid);
    }
  }
  return new ToolCard(null, '-1');
}
