import { Arm, ArmCard, ArmsTypes } from "src/app/models/card.model";
import { CardSaveData } from "src/app/models/player-data.model";

export function getArm(saveData: CardSaveData): ArmCard {
  switch (saveData.id.substr(2, 3)) {
    case ('001'): {
      let arm: Arm = {
        name: "Steel Axe",
        image:'/assets/images/arms/Steel_Axe.png',
        id: "03001",
        armsType: ArmsTypes.AXE,
        uses: 5,
        minRange: 0,
        maxRange: 1,
        damage: 15,
        description: 'An exceptionally sharp and durable Axe.'
      }
      return new ArmCard(arm, saveData.uuid);
    }
    case ('002'): {
      let arm: Arm = {
        name: "Hex",
        id: "03002",
        armsType: ArmsTypes.DARKMAGIC,
        uses: 5,
        minRange: 0,
        maxRange: 1,
        damage: 10
      }
      return new ArmCard(arm, saveData.uuid);
    }
  }
  return new ArmCard(null, '-1');
}
